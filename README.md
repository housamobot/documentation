#Housamobot

Housamobot is a Discord bot written in Python 3. Its main purpose is to fetch
gameplay information from the [Housamo wiki](https://housamo.wiki/Main_Page),
although it also provides certain miscellaneous utility or fun commands.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Commands

Housamobot supports a variety of commands. Some, such as .help, are handled
using discord.py's command API. Others, namely the fetch commands to retrieve
information from the wiki, are parsed with a custom function.

### Fetch Commands

Fetch commands are those enclosed in double brackets, and they are of the form:

```[[search_term toggle:toggle_value subcommand]]```

These commands can be placed anywhere in the user's message, but only the first 
such command will trigger a search and respective output. Names separated by
spaces are available, and certain parameters may be omited, depending on the
type of search. Each type of search has its own syntax, which must be respected 
for the best results.

In the following section, the command syntax for the five supported types of
search (transient, mob, AR equipment, skill, status) are explored in more 
detail. In all cases, parameters between parenthesis are optional and may be
omitted, with the bolded parameter values being passed by default.

#### Transient Search

**Syntax:** ```[[transient_name (rarity:3|4|5|**higher_rarity**) (**ss**/portrait)]]```

Searches for a transient, either released or unreleased. Obviously, unreleased
names are not fully supported, due to missing information in the wiki itself.
Transient name should be separated by spaces, and includes alternate characters, 
e.g. Talos. Certain fun or useful nicknames, e.g. Zab, are also supported.


Rarities for the regular unit can be specified; if the user is not sure
about whether the unit's higher rarity is a 4☆ or 5☆, this parameter can be
omitted or set to ```higher_rarity``` instead. To search for the unit's variant,
the rarity should be set to ```variant``` instead. 

By default Housamobot will fetch a screenshot of the transient's infobox (ss), 
which includes their gameplay-related information, such as statistics, skills,
and details on their Charge Skill. The user may also fetch portraits, which
are the transient's artwork (or sprite) for the defined rarity. At this time,
aside from alternate characters, there is no support for fetching different 
skins or expressions.


----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
